module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  reporters: [
    'default',
    [
      'jest-html-reporters',
      {
        publicPath: './jest',
        filename: 'reporter.html',
        expand: 'true',
      },
    ],
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{js,ts,vue}',
    '!src/**/*.component.ts',
    '!src/main.ts',
    '!src/router/index.ts',
    '!src/secondary/store/index.ts',
    '!**/*.d.ts',
  ],
  coverageReporters: ['html', 'json-summary', 'text-summary', 'lcov', 'clover'],
  coverageDirectory: './jest',
};
