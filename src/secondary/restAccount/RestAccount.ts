import { Account } from '@/domain/account/Account';

export interface RestAccount {
  username: string;
  birthdate: Date;
  profile_picture: string;
}

export const toAccount = (restAccount: RestAccount): Account => ({
  username: restAccount.username,
  birthdate: restAccount.birthdate,
  picture: restAccount.profile_picture,
});
