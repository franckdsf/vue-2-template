import { Account } from './Account';

export interface AccountRepository {
  get(username: string): Promise<Account>;
  signIn(username: string, password: string): Promise<boolean>;
}
