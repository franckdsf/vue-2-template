export interface Account {
  username: string;
  birthdate: Date;
  picture: string;
}
