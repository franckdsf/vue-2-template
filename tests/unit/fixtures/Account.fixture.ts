import { Account } from '@/domain/account/Account';
import { RestAccount } from '@/secondary/restAccount/RestAccount';

export const fixtureRestAccount = (): RestAccount => ({
  username: 'franckdsf',
  birthdate: new Date('10/10/1999'),
  profile_picture: '/pictures/37817.png',
});

export const fixtureAccount = (): Account => ({
  username: 'franckdsf',
  birthdate: new Date('10/10/1999'),
  picture: '/pictures/37817.png',
});
