import { fixtureRestAccount, fixtureAccount } from '../../fixtures/Account.fixture';
import { stubAxiosInstance, axiosError } from '../../TestUtils';

import { RestAccountRepository } from '@/secondary/restAccount/RestAccountRepository';

describe('RestAccountRepository', () => {
  it('Should fail to retrieve account informations', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.rejects(axiosError());
    const restAccountRepository = new RestAccountRepository(axiosInstance);

    await restAccountRepository.get('franckdsf').catch(error => {
      expect(error).toEqual(Error("Can't get account: Network Error"));
    });
  });
  it('Should successfully retrieve account informations', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.resolves({ data: fixtureRestAccount() });
    const restAccountRepository = new RestAccountRepository(axiosInstance);

    const result = await restAccountRepository.get('franckdsf');
    expect(result).toStrictEqual(fixtureAccount());
  });
  it('Should fail to sign into an account', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.post.rejects(axiosError(500, 'Network Error'));
    const restAccountRepository = new RestAccountRepository(axiosInstance);

    await restAccountRepository.signIn('franckdsf', 'test').catch(error => {
      expect(error).toEqual(Error("Can't sign in: Network Error"));
    });
  });
  it('Should fail to sign into an account with incorrect credentials', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.post.rejects(axiosError(402, 'Your credentials may be incorrect.'));
    const restAccountRepository = new RestAccountRepository(axiosInstance);

    await restAccountRepository.signIn('franckdsf', 'test').catch(error => {
      expect(error).toEqual(Error("Can't sign in: email or password incorrect"));
    });
  });
  it('Should successfully retrieve account informations', async () => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.post.resolves({ data: true });
    const restAccountRepository = new RestAccountRepository(axiosInstance);

    const result = await restAccountRepository.signIn('franckdsf', 'test');
    expect(result).toBe(true);
  });
});
